import ForgeUI, { render, useState, useProductContext } from "@forge/ui";
import { useContentProperty } from "@forge/ui-confluence";

import { Metadata, EntryData } from "./types";
import SignupView from "./SignupView";
import ConfigView from "./ConfigView";

const App = () => {
  const [metadata, updateMetadata] = useContentProperty<Metadata>("metadata", {
    hasPublished: false,
    fields: [],
    title: ""
  });
  const [isEditing, setIsEditing] = useState(false);
  const [, updateEntries] = useContentProperty<EntryData[]>("entries", []);

  return metadata.hasPublished && !isEditing ? (
    <SignupView
      fields={metadata.fields}
      title={metadata.title}
      revertToConfig={() => setIsEditing(true)}
    />
  ) : (
    <ConfigView
      existingConfig={{ fields: metadata.fields, title: metadata.title }}
      setConfig={async ({ fields, title }) => {
        setIsEditing(false);
        await updateMetadata({
          fields: fields.filter(
            field => !(!field.label || !field.label.trim())
          ),
          hasPublished: true,
          title
        });
        await updateEntries(entries => {
          return entries.map(entry => {
            const newEntry = {
              name: entry.name,
              accountId: entry.accountId
            };
            fields.forEach(field => {
              if (!(field.label in ["name", "accountId"])) {
                newEntry[field.label] = entry[field.label];
              }
            });
            return newEntry as EntryData;
          });
        });
      }}
    />
  );
};

export const run = render(<App />);
